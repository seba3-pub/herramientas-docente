#!/usr/bin/python3

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import random
import re


tipo_pregunta_default = 's'
patron_pregunta_texto = '^\[(\d+)\]'
PAGINA = 23941 #cualquier objeto
HTML = 23942 #cualquier otro objeto


def incluir(disenio, array):
    preguntas = disenio["preguntas"]
    opciones = []
    enunciado = ''
    tipo_pregunta = ''

    for line in array:
        cerrar_pregunta = False
        l = line.strip()
        if l != '':
            if l[0] == '#':
                pass
            elif l[0] == '<':
                preguntas.append( [HTML, l] )
            elif l[0] == '$':
                s = l[1:].split(':')
                if len(s) != 2:
                    print('Error de formato en %s' %l)
                    sys.exit(1)
                variable = s[0]
                valor = s[1]
                if variable == 'titulo':
                    disenio['titulo'] = valor
                elif variable == 'ejemplares':
                    disenio['ejemplares'] = int(valor)
                elif variable == 'pie':
                    disenio['pie'] = valor
                elif variable == 'encabezado':
                    disenio['encabezado'] = valor
                elif variable == 'pagina':
                    preguntas.append(PAGINA)
                elif variable == 'css':
                    disenio['css'] = valor
                elif variable == 'incluir':
                    array2 = []
                    f = open(valor.strip())
                    for line2 in f:
                        array2.append(line2)
                    f.close()
                    array2.append('')

                    incluir(disenio, array2)
                elif variable == 'incluir_html':
                    array2 = []
                    f = open(valor.strip())
                    for line2 in f:
                        array2.append(line2.strip())
                    f.close()
                    texto = '\n'.join(array2)
                    preguntas.append( [HTML, texto] )
            elif enunciado:
                opcion = l
                if tipo_pregunta == 't':
                    if not ('[]' in opcion):
                        opcion = '[]%s' % opcion
                opciones.append(opcion)
            else:
                i = 0
                pregunta_text = re.search(patron_pregunta_texto, l)
                espacio_completar = 0
                if(pregunta_text):
                  tipo_pregunta = 't'
                  espacio_completar = int(pregunta_text.group(1))
                  [j, i] = pregunta_text.span()
                while i < len(l):
                    if l[i] in '[]':
                        tipo_pregunta = 'm'
                    elif l[i] in '()':
                        tipo_pregunta = 's'
                    elif l[i] in '--':
                        tipo_pregunta = 'n'
                        cerrar_pregunta = True
                    else:
                        break
                    i = i + 1
                if i < len(l):
                    enunciado = l[i:]
        else:
            if opciones:
                if tipo_pregunta == '':
                    tipo_pregunta = tipo_pregunta_default
                cerrar_pregunta = True
        if cerrar_pregunta:
            preguntas.append([tipo_pregunta, enunciado, opciones, espacio_completar])
            enunciado = ''
            opciones = []
            tipo_pregunta = ''
    if opciones:
        if tipo_pregunta == '':
            tipo_pregunta = tipo_pregunta_default
        preguntas.append([tipo_pregunta, enunciado, opciones])


def generar(disenio):

    print('<!DOCTYPE html>')
    print('<head>')
    print('<meta charset="utf-8">')
    print('<link rel="stylesheet" href="examen.css">')
    if disenio['css']:
        print('<link rel="stylesheet" href="%s">' % disenio['css'])
    print('</head>')
    print('\n<body>')
    for ejemplar in range(0, disenio['ejemplares']):
        print('<div class="alumno">Alumno:</div>')
        print('<div class="ejemplar">')
        print('<h1>%s</h1>' % disenio['titulo'])
        if disenio['encabezado']:
            print('<div class="encabezado">%s</div>' % disenio['encabezado'])
        print('<ol>')
        preguntas = disenio['preguntas']
        for i in range(0, len(preguntas)):
            if preguntas[i] == PAGINA:
                print('<div class="pagina"></div>')
                continue
            if preguntas[i][0] == HTML:
                print(preguntas[i][1])
                continue
            [tipo_pregunta, enunciado, opciones, espacio_completar] = preguntas[i]
            if tipo_pregunta == 's':
                sufijo_clase = 'simple'
            elif tipo_pregunta == 'm':
                sufijo_clase = 'multiple'
            elif tipo_pregunta == 't':
                sufijo_clase = 'text'
            elif tipo_pregunta == 'n':
                sufijo_clase = 'sin_opciones'
            else:
                sufijo_clase = None
            if sufijo_clase:
                clase = 'class="pregunta pregunta_%s"' % sufijo_clase
            else:
                clase = ''
            print('\n\n<li %s>%s</li>' % (clase, enunciado))
            if tipo_pregunta != 'n':
                random.shuffle(opciones)
                extra = ''
                if tipo_pregunta == 't':
                    extra = '<span class="espacio_text" style="padding: 0 %sem 0 0">&nbsp;</span>' % espacio_completar
                print('<ul %s>' % clase)
                if sufijo_clase:
                    clase = 'class="opcion opcion_%s"' % sufijo_clase
                for j in range(0, len(opciones)):
                    o = opciones[j]
                    if tipo_pregunta == 't':
                        o = o.replace('[]', extra)
                    print('    <li %s><span>%s</span></li>' % (clase, o))
                print('</ul>')
        print('</ol>')
        if disenio['pie']:
            print(disenio['pie'])
        print('</div>')
    print('</body>')



if len(sys.argv) > 1:
    entrada = open(sys.argv[1])
else:
    entrada = sys.stdin


disenio = {
    'titulo': 'Examen',
    'encabezado': '',
    'pie': '',
    'ejemplares': 1,
    'preguntas': [],
    'css': None
}


array = []
for line in entrada:
    array.append(line)
entrada.close()
array.append('')

incluir(disenio, array)

random.seed()
generar(disenio)

